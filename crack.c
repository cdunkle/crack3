#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
typedef struct entry 
{
    char plainPassword[512]; 
    char hashedPassword[100]; 
} entry;



// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    int arrayLength = 4;
    
    //opens file
    FILE *fileBeingRead = fopen(filename, "r");
    
    //checks to make sure that the file exists
    if(!fileBeingRead)
    {
        perror("Can't open Dictionary");
        exit(1);
    }
    //Array of structs
    entry *arr = malloc(arrayLength * sizeof(entry));
    
    //string for line in file
    char line[50];
    
    //index position for our arr array
    int index = 0;
    
    //loop to go through the file and put the info into the structures
    while(fgets(line, 50, fileBeingRead) != NULL)
    {
        //This will reallocate memory if needed (I believe)
        if(index == arrayLength)
        {
            arrayLength += 4;
            arr = realloc(arr, arrayLength * sizeof(entry));
        }
        //stringlength of the line
        int stringlen = strlen(line);
        
        //cut out the newline char
        line[stringlen - 1] = '\0';
        
        //copies the line to the plaintext 
        strcpy((arr[index].plainPassword), line);
        strcpy((arr[index].hashedPassword), md5(line, strlen(line)));
        
        //use this to make sure that our hashes are coming out correctly
        //printf("Plain: %s\nHashed: %s\n", arr[index].plainPassword, arr[index].hashedPassword);
        
        //increment loop index
        index++;
    }
    
    //closes the file
    fclose(fileBeingRead);
    //Use this to make sure we output the correct amount of entries
    //printf("Count: %d\n", index);
    
    *size = index;
    return arr;
}

//Our comparison function for our qsort
int compHash(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    
    return strcmp((*aa).hashedPassword, (*bb).hashedPassword);
}

//Binary Search Comparison Function
int searchByName(const void *t, const void *elem)
{
    char *tt = (char *)t;
    struct entry *eelem = (struct entry *)elem;
    
    return strcmp(tt, eelem->hashedPassword);
}

//Print Structure Function
void printEntry(struct entry a)
{
    printf("%s %s\n", a.plainPassword, a.hashedPassword);
}



int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    int entrySize;
    
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &entrySize);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, entrySize, sizeof(entry), compHash);
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    //struct entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, entrySize, sizeof(struct entry), searchByName);
    
    // TODO
    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    
    char line[50];
    int loopCounter = 1;
    int foundCounter = 0;
    while(fgets(line, 50, hashFile) != NULL)
    {
        int lineLength = strlen(line);
        line[lineLength - 1] = '\0';
        struct entry *found = bsearch(line, dict, entrySize, sizeof(struct entry), searchByName);
        if(found)
        {
        
            printf("Found It!\n");
            printEntry(dict[loopCounter - 1]);
            foundCounter++;
        }
        loopCounter++;
    }
    printf("Items Found: %d\n", foundCounter);
}
